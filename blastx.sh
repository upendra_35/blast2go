#!/bin/bash

for i in *.fasta; do blastx -db ../blastdb/nr -query $i -out $i.annot.xml -evalue 0.000001 -num_threads 48 -outfmt 5 ; done