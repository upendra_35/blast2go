#!/bin/bash

echo -e "\n\n### Get NCBI nr database files ###"
mkdir -p ./blastdb &&
cd ./blastdb &&
wget ftp://zhanglab.ccmb.med.umich.edu/download/nr.tar.gz
tar -xvf nr.tar.gz

ls -alh . &&
cd ..

echo -e "\nDone!"

#EOF