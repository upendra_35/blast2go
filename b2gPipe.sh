#!/bin/bash
echo -e "\n\n### Get Blast2GO B2G4Pipe ###"
mkdir -p ./blast2go &&
cd ./blast2go &&
wget http://www.blast2go.com/data/blast2go/b2g4pipe_v2.5.zip
unzip b2g4pipe_v2.5.zip
cd ./b2g4pipe

echo -e "\nDone!"

#EOF 