#!/usr/bin/env perl

use strict;
use Bio::SeqIO;

my $from = shift;
my $toprefix = shift;
my $seqs = shift;

my $in  = new Bio::SeqIO(-file  => $from);

my $count = 0;
my $fcount = 1;
my $fa = "fasta";
my $out = new Bio::SeqIO(-file => ">$toprefix.$fcount.$fa", -format=>'fasta');
while (my $seq = $in->next_seq) {
        if ($count % $seqs == 1) {
                $fcount++;
                $out = new Bio::SeqIO(-file => ">$toprefix.$fcount.$fa", -format=>'fasta');
        }
        $out->write_seq($seq);
        $count++;
}